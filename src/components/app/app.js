import React, {Component} from 'react';
import AppHeader from "../app-header";
import SearchPannel from "../search-pannel";
import TodoList from "../todo-list";
import ItemAddForm from "../item-add-form";
import ItemStatusFilter from '../item-status-filter';

import "./app.css";

export default class App extends Component {

  maxId = 100;

  state = {
    todoData: [
      this.createTodoItem("Drink coffee"),
      this.createTodoItem("Create react component"),
      this.createTodoItem("To build todo list"),
    ],
    term: "",
    filter: "all",
  };

  createTodoItem(label) {
    return {
      label,
      important: false,
      done: false,
      id: this.maxId++
    }
  };

  deleteItem =(id) => {
    this.setState (({todoData}) => {
      const index = todoData.findIndex((el) => el.id === id);
      const before = todoData.slice(0, index);
      const after = todoData.slice(index+1);

      const newArray = [...before, ...after];

      return {
        todoData: newArray
      }
    })
  };

  addItem = (text) => {
    const newItem = this.createTodoItem(text);

    this.setState (({todoData}) => {
      const newArr = [
        ...todoData,
        newItem
      ]
      return {
        todoData: newArr
      }
    })
  };

  onToggleProperty = (arr, id, propName) => {
    const index = arr.findIndex((el) => el.id === id);
      const oldItem = arr[index];
      const newItem = {...oldItem, [propName]: !oldItem[propName]};
    return [
        ...arr.slice(0, index),
        newItem,
        ...arr.slice(index+1),
      ]
  };

  onToggleDone = (id) => {
    this.setState (({todoData}) => {
      return {
        todoData: this.onToggleProperty(todoData, id, "done")
      }
    })
  };

  onToggleImportant = (id) => {
    this.setState ( ({todoData})=> {
      // const index = todoData.findIndex((el) => el.id === id);
      // const oldData = todoData[index];
      // const newData = {...oldData, important: !oldData.important};

      // const newArray =  [
      //   ...todoData.slice(0, index),
      //   newData,
      //   ...todoData.slice(index+1)
      // ]

      return {
        todoData: this.onToggleProperty(todoData, id, "important")
      }

    })
  };

  onSearchChange = (term) => {
    this.setState ({term});
  };

  onFilterChange = (filter) => {
    this.setState({filter});
  }

  search (items, term) {
    if (term.length === 0) {
      return items
    };

    return items.filter ((item) => {
      return item.label.toLowerCase().indexOf(term.toLowerCase()) > -1;
    })
  };

  filter(items, filter) {
    switch (filter) {
      case "all":
        return items;
      case "active":
        return items.filter((item) => !item.done);
      case "done":
        return items.filter((item) => item.done);
      default:
        return items;
    }
  }


  render() {
    const {todoData, term, filter} = this.state;

    // const visibleData = this.search(todoData, term);
    const visibleData = this.filter(this.search(todoData, term), filter);
    const doneCount = todoData.filter((el) => el.done).length;
    const todoCount = todoData.length - doneCount;

    return (
      <div>
        <AppHeader todo={todoCount} done={doneCount}/>
        <div className="connection">
          <SearchPannel
            onSearchChange = {this.onSearchChange}/>
          <ItemStatusFilter filter={filter}
            onFilterChange={this.onFilterChange}/>
        </div>
        <TodoList 
          todos={visibleData} /* todos={todoData} */
          onDeleted={this.deleteItem}
          onToggleDone={this.onToggleDone}
          onToggleImportant={this.onToggleImportant}/>
        <ItemAddForm
          onAdded={ this.addItem}/>
      </div>
    );
  }
}