import React, {Component} from 'react';

import "./search-pannel.css";

export default class SearchPannel extends Component {
  
  state = {
    term: ""
  }

  onSearchChange = (e) => {
    const term = e.target.value;
    this.setState ({term});
    this.props.onSearchChange(term);
  }

  render() {
    return(
      <div className="search-pannel-area">
        <input 
          placeholder="search" 
          className="search-pannel"
          value = {this.state.term}
          onChange = {this.onSearchChange}/>
      </div>
    );
  }
}
