import React, {Component} from 'react';
import "../todo-list-item/todo-list-item.css";
import Delete from '@material-ui/icons/Delete';
import PriorityHighIcon from '@material-ui/icons/PriorityHigh';

export default class TodoListItem extends Component {
/*   state = {
    done: false,
    important: false,
  }

  onLabelClick = () => {
    this.setState ( ({done}) => {
      return {
        done: !done
      }
    })
  };

  onMarkClick =() => {
    this.setState ( ({important}) => {
      return {
        important: !important
      }
    })
  };
 */
  render () {

    const {label, onDeleted, onToggleDone, onToggleImportant, done, important} = this.props;
/*     const {done, important} = this.state; */

   let className = "todo-list-item";

    if (done) {
      className += " done"
    };

    if (important) {
      className += " important"
    };
  
    return(
      <div className="todo-list-item"> 
        <span 
         className={className}
          onClick = {onToggleDone}>
            {label}
        </span>
        <div className="button-area">
          <button type="button"
                  className="btn btn-outline-danger btn-sm button-margin" 
                  onClick= {onDeleted}>
            <Delete/>
          </button>
          <button type="button"
                  className="btn btn-outline-success btn-sm" 
                  onClick = {onToggleImportant}>
            <PriorityHighIcon />
          </button>
        </div>
      </div>
    );
  }
}
